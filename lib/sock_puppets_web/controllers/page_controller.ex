defmodule SockPuppetsWeb.PageController do
  use SockPuppetsWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
