defmodule Mix.Tasks.Tools.Up do
  use Mix.Task

  @shortdoc "set up dev. tools for sock_puppets"
  def run(_args) do
    Mix.shell.cmd "docker-compose -f lib/mix/tasks/tools/docker-compose.yml up -d"
  end

end
