defmodule Mix.Tasks.Tools.Down do
  use Mix.Task

  @shortdoc "remove dev. tools for sock_puppets"
  def run(_args) do
    Mix.shell.cmd "docker-compose -f lib/mix/tasks/tools/docker-compose.yml down"
  end

end
